import { Injectable } from '@angular/core';
import { IUser } from './user.model';

@Injectable()
export class AuthService {
    curentUser:IUser

    loginUser(userName: string, password: string) {
        this.curentUser = {
            id: 1,
            userName: userName,
            firstName: 'Johny',
            lastName: 'Papa'
        }
    }

    isAuthenticated() {
        return !!this.curentUser;
    }

    updateCurrentUser(firstName:string, lastName:string) {
        this.curentUser.firstName = firstName
        this.curentUser.lastName = lastName
    }
}